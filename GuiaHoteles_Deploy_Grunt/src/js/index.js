$(function(){ // function que activa los tooltip
  $('[data-toggle="tooltip"]').tooltip({delay: { "show": 500, "hide": 500 }});
  $('[data-toggle="popover"]').popover({delay: { "show": 300, "hide": 300 }});
  $('.carousel').carousel({interval: 2000});

  //suscribiendo al evento de modal
  $('#modal-contact').on('show.bs.modal', function(e){
      console.log("se va a visualizar el modal mediante 'show.bs.modal'");

      //e.relatedTarget.id  -> contiene el id del elemento que lanzo el modal
      //desabilitar buttom cuando se le hizo click
      $('#' + e.relatedTarget.id).removeClass('btn-outline-primary');
      $('#' + e.relatedTarget.id).addClass('btn-outline-secondary');
  });
  $('#modal-contact').on('shown.bs.modal', function(e){
      console.log('Se esta visualizando el modal mediante "shown.bs.modal"');
  });
  $('#modal-contact').on('hidden.bs.modal', function(e){ //objeto e asociado al evento
      console.log('Se oculta el evento');
  });

  
});