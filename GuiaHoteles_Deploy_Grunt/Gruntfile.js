module.exports = function(grunt){

  require('time-grunt')(grunt);
  /*require('../GuiaHoteles_Deploy_Grunt/node_modules/jit-grunt/jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  });*/
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  });
  grunt.initConfig({
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'src/styles/sass/',
          src: ['*.scss'],
          dest: 'src/styles/css/',
          ext: '.css'
        }]
      }
    },

    watch: {
      files: ['src/styles/sass/*.scss'],
      tasks: ['css']
    },

    browserSync: {
      dev: {
        bsFiles: {
          src: [
            'src/styles/css/*.css',
            '*.html', 'src/html/*.html',
            'src/js/*.js'
          ]
        },
        options: {
          watchTask: true,
          server: {
            baseDir: './'  //directorio base para nuestro servidor
          }
        }
      }
    },

    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd:'src/images/',
          src:'*.{png, gif, jpg, jpeg}',
          dest: 'dist/src/images/'
        }]
      }
    },

    copy: {
      html: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: './',
            src: ['*.html'],
            dest: 'dist/'
          },
          {
            expand: true,
            cwd: 'node_modules/open-iconic/font/fonts',
            src: '*.*',
            dest: 'dist/src/fonts'
          },
        ]
      },
      html_src: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: './src/html/',
            src: ['*.html'],
            dest: 'dist/src/html'
          }
        ]
      }
    },  

    clean: {
      build: {
        src: ['dist']
      }
    },

    cssmin: {
      dist: {}
    },

    uglify: {
      dist: {}
    },

    filerev: {
      options: {
        enconding: 'utf8',
        algorithm: 'md5',
        length: 20
      },
      release: {
        files: [{
          src: [ /*
            'dist/src/js/*.js',
            'dist/src/css/*.css' */
          ]
        }]
      }
    },

    concat: {
      options: {
        separator: ';'
      },
      dist: {}
    },

    useminPrepare: {
      foo: {
        dest: ['dist'],
        src: ['index.html']
      },
      bar: {
        src: ['about.html','contact.html', 'precios.html'],
        dest: ['dist/src/html']
      },
      options: {
        flow: {
          steps: {
            css: ['cssmin'],
            js: ['uglify']
          },
          post: {
            css: [{
              name: 'cssmin',
              createConfig: function(context, block){
                var generated = context.options.generated;
                generated.options = {
                  keepSpecialComments: 0,
                  rebase: false
                }
              }
            }]
          }
        }
      }
    },

    usemin: {
      html: ['dist/index.html'],
      options: {
        assetsDir: ['dist', 'dist/src/css', 'dist/src/js', 'dist/src/fonts']
      },
      html_src: ['dist/src/html/about.html', 'dist/src/html/contact.html', 'dist/src/html/precios.html'],
      options: {
        assetsDir: ['dist/src/css', 'dist/src/js', 'dist/src/fonts', 'dist/src/html']
      }
    } 
  });

  //agregar las task(tareas)
  
  /*grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-imagemin'); */
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.registerTask('cp', ['copy'])
  grunt.registerTask('css', ['sass']);
  grunt.registerTask('default', ['browserSync', 'watch']);
  grunt.registerTask('img:compress',['imagemin'] );
  grunt.registerTask('build', [
    'clean',
    'copy',
    'imagemin',
    'useminPrepare',
    'concat',
    'cssmin',
    'uglify',
    'filerev',
    'usemin'
  ]);
}