'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify-es').default,
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');
  
const { series, parallel } = require('gulp');

gulp.task('sass', ()=>{
  return new Promise((resolve, reject)=>{
    gulp.src('./src/styles/sass/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./src/styles/css/'));
    resolve();
    //falta el reject
  });
});

gulp.task('sass:watch', ()=>{
  gulp.watch('./src/styles/sass/*.scss', series('sass'));
  //cuando cambia *.scss se ejecuta la tarea sass indicada con 'series'
});

gulp.task('browser-sync',()=>{
  var files = ['./*.html', './src/styles/css/*.css', './src/images/*.{png, jpg, gif}', './src/html/*.html', './src/js/*.js'];
  browserSync.init(files, {
    server: {
      baseDir: './'
    }
  });
});

//parallel ejecuta en paralelo las tareas pasadas
gulp.task('default', parallel('browser-sync', 'sass:watch'));

gulp.task('clean', ()=>{
  return del(['dist']);
});

gulp.task('imagemin', ()=>{
  return gulp.src('./src/images/*.{png, jpg, jpeg, gif}')
              .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
              .pipe(gulp.dest('./dist/src/images'));
});

gulp.task('usemin:index', ()=>{
  return gulp.src('./*.html')
              .pipe(flatmap((stream, file)=>{
                return stream
                        .pipe(usemin({
                          css: [rev()],
                          html: [()=>{ return htmlmin({collapseWhitespace: true})}],
                          js: [uglify(), rev()],
                          inlinejs: [uglify()],
                          inlinecss: [cleanCss(), 'concat']
                        }));
              }))
              .pipe(gulp.dest('./dist/'));
});

gulp.task('usemin:src', ()=>{
  return gulp.src('./src/html/*.html')
              .pipe(flatmap((stream, file)=>{
                return stream
                        .pipe(usemin({
                          css: [rev()],
                          html: [()=>{ return htmlmin({collapseWhitespace: true})}],
                          js: [uglify(), rev()],
                          inlinejs: [uglify()],
                          inlinecss: [cleanCss(), 'concat']
                        }));
              }))
              .pipe(gulp.dest('./dist/src/html/'));
});

gulp.task('copyfonts', ()=>{
  return new Promise((resolve, reject)=>{
    gulp.src('./node_modules/open-iconic/font/fonts/*.*')
      .pipe(gulp.dest('./dist/src/fonts'));
    resolve();
  });
});

gulp.task('build', series('clean','copyfonts','imagemin', 'usemin:index', 'usemin:src'));